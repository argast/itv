name := "itv"

version := "1.0"

scalaVersion := "2.13.0"

libraryDependencies ++= Seq(
  "com.beachape" %% "enumeratum" % "1.5.13",
  "org.scalatest" %% "scalatest" % "3.0.8" % "test"
)