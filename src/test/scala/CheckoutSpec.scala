import Items.{A, B}
import org.scalatest.{Matchers, WordSpec}

class CheckoutSpec extends WordSpec with Matchers {

  val offer = new Offer {
    override def applyOffer(items: Seq[Item]): Seq[Item] = items
    override def toString: String = "Test offer"
  }

  "Checkout" should {
    "scan item and add it to items list" in {
      Checkout.initial.scan(A) shouldEqual Checkout(Seq(A), Seq.empty)
    }

    "add offer to offer list" in {
      Checkout.initial.add(offer) shouldEqual Checkout(Seq.empty, Seq(offer))
    }

    "remove offer from offer list" in {
      val checkoutWithOffer = Checkout.initial.add(offer)
      checkoutWithOffer.remove(offer) shouldEqual Checkout.initial
    }

    "print correct status" in {
      Checkout.initial
        .add(offer)
        .scan(B)
        .status shouldEqual "Price: 30.0, Items: B, Offers: Test offer"

      Checkout.initial.status shouldEqual "Price: 0.0, Items: None, Offers: None"
    }
  }

}
