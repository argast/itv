import java.io.{ByteArrayOutputStream, StringReader}

import org.scalatest.{Matchers, WordSpec}

class MainSpec extends WordSpec with Matchers {

  "Checkout program" should {
    "run commands and print correct output" in {
      val output = runProgramWithInput(
        """scan A
          |scan B
          |scan C
          |scan A
          |scan A
          |add A 3 for 130
          |add B 2 for 45
          |scan B
          |scan A
          |remove A 3 for 130
          |remove B 2 for 45
          |end
        """.stripMargin)

      output shouldEqual
        """Use following commands:
          |scan <item> e.g. scan A - scan one item
          |add <item> <quantity> for <price> e.g. add A 3 for 130 - add offer
          |remove <item> <quantity> for <price> e.g. remove A 3 for 130 - remove offer
          |end - finish checkout session
          |Price: 0.0, Items: None, Offers: None
          |Next command: Price: 50.0, Items: A, Offers: None
          |Next command: Price: 80.0, Items: A, B, Offers: None
          |Next command: Price: 100.0, Items: A, B, C, Offers: None
          |Next command: Price: 150.0, Items: A, B, C, A, Offers: None
          |Next command: Price: 200.0, Items: A, B, C, A, A, Offers: None
          |Next command: Price: 180.0, Items: A, B, C, A, A, Offers: A 3 for 130.0
          |Next command: Price: 180.0, Items: A, B, C, A, A, Offers: A 3 for 130.0, B 2 for 45.0
          |Next command: Price: 195.0, Items: A, B, C, A, A, B, Offers: A 3 for 130.0, B 2 for 45.0
          |Next command: Price: 245.0, Items: A, B, C, A, A, B, A, Offers: A 3 for 130.0, B 2 for 45.0
          |Next command: Price: 265.0, Items: A, B, C, A, A, B, A, Offers: B 2 for 45.0
          |Next command: Price: 280.0, Items: A, B, C, A, A, B, A, Offers: None
          |Next command: Price: 280.0, Items: A, B, C, A, A, B, A, Offers: None
          |""".stripMargin
    }
  }

  def runProgramWithInput(input: String): String = {
    val in = new StringReader(input)
    val out = new ByteArrayOutputStream()
    Console.withIn(in) {
      Console.withOut(out) {
        Main.main(Array())
        out.close()
        in.close()
        out.toString
      }
    }
  }
}
