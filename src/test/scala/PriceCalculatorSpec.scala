import org.scalatest.{Matchers, WordSpec}

import Items._

class PriceCalculatorSpec extends WordSpec with Matchers {

  "PriceCalculator" should {
    "return 0 from empty list of items" in {
      PriceCalculator.totalPrice(Seq()) shouldEqual 0.0
    }

    "calculate correct total price for items without offers" in {
      PriceCalculator.totalPrice(Seq(A, B, C, D)) shouldEqual (50.0 + 30.0 + 20.0 + 15.0)
    }

    "calculate correct total price for items with single offer" in {
      val offers = Seq(QuantityOffer(B, 2, 45.0))
      val items = Seq(A, B, A, B, A, A, C, D)
      PriceCalculator.totalPrice(items, offers) shouldEqual (50.0 + 50.0 + 50.0 + 50.0 + 45.0 + 20.0 + 15.0)
    }

    "calculate correct total price for items with multiple offers" in {
      val offers = Seq(QuantityOffer(A, 3, 130.0), QuantityOffer(B, 2, 45.0))
      val items = Seq(A, B, A, B, A, A, C, D)
      PriceCalculator.totalPrice(items, offers) shouldEqual (130.0 + 50.0 + 45.0 + 20.0 + 15.0)
    }
  }
}
