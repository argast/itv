import Items.A
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{Matchers, WordSpec}

class CommandParserSpec extends WordSpec with Matchers with TableDrivenPropertyChecks {

  "OfferParser" should {
    forAll(Table(
      ("command", "expected result"),
      ("scan A", Right(Scan(A))),
      ("add A 3 for 130", Right(Add(QuantityOffer(A, 3, 130)))),
      ("remove A 3 for 130", Right(Remove(QuantityOffer(A, 3, 130)))),
      ("end", Right(End)),
      ("remove A 3 to 130", Left("Invalid command")),
      ("add Q 3 for 130", Left("Q is not a member of Enum (A, B, C, D)")),
      ("add A a for 130", Left("For input string: \"a\"")),
      (null, Left("Empty command")),
      ("wrong", Left("Invalid command"))
    )) { (command, expectedResult) =>
      s"parse '$command' into $expectedResult" in {
        CommandParser.parse(command) shouldEqual expectedResult
      }
    }
  }
}
