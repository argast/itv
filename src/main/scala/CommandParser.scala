import scala.util.Try

object CommandParser {

  def parse(s: String): Either[String, Command] = {
    if (s == null) Left("Empty command")
    else s.split(" ") match {
      case Array("end") => Right(End)
      case Array("scan", item) => Right(Scan(Items.withName(item)))
      case Array("add", item, quantity, "for", price) => createOffer(item, quantity, price).map(Add.apply)
      case Array("remove", item, quantity, "for", price) => createOffer(item, quantity, price).map(Remove.apply)
      case _ => Left("Invalid command")
    }
  }

  private def createOffer(item: String, quantity: String, price: String): Either[String, Offer] = {
    Try {
      QuantityOffer(Items.withName(item), quantity.toInt, price.toInt)
    }.toEither.left.map(_.getMessage)
  }

}
