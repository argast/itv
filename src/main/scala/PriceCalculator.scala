
object PriceCalculator {

  def totalPrice(items: Seq[Item], offers: Seq[Offer] = Seq.empty): Double = {
    offers.foldLeft(items) { (currentItems, offer) =>
      offer.applyOffer(currentItems)
    }.map(_.price).sum
  }
}
