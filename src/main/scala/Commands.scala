
trait Command {
  def run(checkout: Checkout): Checkout
}

case class Scan(item: Item) extends Command {
  override def run(checkout: Checkout): Checkout = checkout.scan(item)
}

case class Add(offer: Offer) extends Command {
  override def run(checkout: Checkout): Checkout = checkout.add(offer)
}

case class Remove(offer: Offer) extends Command {
  override def run(checkout: Checkout): Checkout = checkout.remove(offer)
}

case object End extends Command {
  override def run(checkout: Checkout): Checkout = checkout
}
