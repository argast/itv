object Main extends App {

  def handleError(error: String): Unit = println(error)

  def read: String = scala.io.StdIn.readLine("Next command: ")

  println(
    """Use following commands:
      |scan <item> e.g. scan A - scan one item
      |add <item> <quantity> for <price> e.g. add A 3 for 130 - add offer
      |remove <item> <quantity> for <price> e.g. remove A 3 for 130 - remove offer
      |end - finish checkout session""".stripMargin
  )

  val finalCheckout = LazyList
    .continually(CommandParser.parse(read)) // read new command
    .takeWhile(_ != Right(End)) // until end is entered
    .foldLeft(Checkout.initial) { (checkout, command) =>
      command match {
        case Right(c) =>
          val newCheckout = c.run(checkout)
          println(newCheckout.status)
          newCheckout

        case Left(error) =>
          handleError(error)
          checkout
      }
    }
  println(finalCheckout.status)
}
