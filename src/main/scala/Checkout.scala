
case class Checkout(items: Seq[Item], offers: Seq[Offer]) {

  def scan(item: Item): Checkout = copy(items = items :+ item)

  def add(offer: Offer): Checkout = copy(offers = offers :+ offer)

  def remove(offer: Offer): Checkout = copy(offers = offers.filterNot(_ == offer))

  private def price = PriceCalculator.totalPrice(items, offers)

  private def itemsString = if (items.isEmpty) "None" else items.mkString(", ")
  private def offersString = if (offers.isEmpty) "None" else offers.mkString(", ")

  def status: String = s"Price: $price, Items: $itemsString, Offers: $offersString"
}

object Checkout {
  def initial = Checkout(Seq.empty, Seq.empty)
}
