import enumeratum._


sealed trait Item extends EnumEntry {
  val price: Double
}

object Items extends Enum[Item] {
  override def values = findValues

  case object A extends Item {
    override val price: Double = 50
  }

  case object B extends Item {
    override val price: Double = 30
  }

  case object C extends Item {
    override val price: Double = 20
  }

  case object D extends Item {
    override val price: Double = 15
  }
}

case class OfferItem(price: Double) extends Item

trait Offer {
  def applyOffer(items: Seq[Item]): Seq[Item]
}

case class QuantityOffer(item: Item, quantity: Int, price: Double) extends Offer {

  def applyOffer(items: Seq[Item]): Seq[Item] = {
    val (offerItems, rest) = items.partition(_ == item)
    val size = offerItems.size
    Seq.fill(size / quantity)(OfferItem(price)) ++ Seq.fill(size % quantity)(item) ++ rest
  }

  override def toString: String = s"$item $quantity for $price"
}
