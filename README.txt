Pawel Poltorak

Project requires sbt to compile and run.

To run all tests:

> sbt test

To run program:

> sbt run


Assumptions:
 - I used enum as items, normally they would be read from DB
 - I interpreted checkout transaction as single scan of an SKU, therefore offers can be added or removed
   between scans